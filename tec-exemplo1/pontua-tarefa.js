
Tarefas = new Meteor.Collection("tarefas");

if (Meteor.isClient) {
  Template.board.tarefas = function () {
    return Tarefas.find({}, {sort: {esforco: -1, name: 1}});
  };

  Template.board.nomeTarefaSelecionada = function () {
    var tarefa = Tarefas.findOne(Session.get("tarefaSelecionada"));
    return tarefa && tarefa.name;
  };
  
  Template.board.valorSelecionado = function (esforco) {
    var tarefa = Tarefas.findOne(Session.get("tarefaSelecionada"));
    return tarefa && tarefa.esforco==esforco;
  };

  Template.tarefa.selected = function () {
    return Session.equals("tarefaSelecionada", this._id) ? "selected" : '';
  };

  Template.board.events({
    'click input.inc': function (event, template) {
      Tarefas.update(Session.get("tarefaSelecionada"),  {$set: {esforco: parseInt(event.target.value) }});
    }
  });

  Template.tarefa.events({
    'click': function () {
      Session.set("tarefaSelecionada", this._id);
    }
  });
}

// On server startup, create some tarefas if the database is empty.
if (Meteor.isServer) {
  Meteor.startup(function () {
    if (Tarefas.find().count() === 0) {
      var names = ["Cadastro de Modelos",
                   "Relatorio de Dealers",
                   "Exportar Planilha X",
                   "Importacao de Dados Y",
                   "Novo Menu"];
      for (var i = 0; i < names.length; i++)
        Tarefas.insert({name: names[i], esforco:0});
    }
  });
}


